// ==UserScript==
// @name        Go to recent Steam game achievements page
// @namespace   xpaw-go-to-recent-game-achievements
// @match       https://steamcommunity.com/*
// @grant       none
// @version     1.0
// @author      xPaw
// ==/UserScript==

const popup = document.querySelector( '#account_dropdown .popup_body' );

const optionsLink = document.createElement( 'a' );
optionsLink.target = '_blank';
optionsLink.className = 'popup_menu_item';
optionsLink.textContent = 'Last Game Achievements';
optionsLink.href = '#';
optionsLink.addEventListener( 'click', ( e ) =>
{
	e.preventDefault();

	optionsLink.textContent = 'Loading…';

	const applicationConfigElement = document.getElementById( 'application_config' );
	const applicationConfig = JSON.parse( applicationConfigElement.dataset.config );
	const userInfo = JSON.parse( applicationConfigElement.dataset.userinfo );
	const accessToken = JSON.parse( applicationConfigElement.dataset.loyalty_webapi_token );

	const params = new URLSearchParams();
	params.set( 'format', 'json' );
	params.set( 'access_token', accessToken );
	params.set( 'count', '1' );
	params.set( 'steamid', userInfo.steamid );

	fetch( `${applicationConfig.WEBAPI_BASE_URL}IPlayerService/GetRecentlyPlayedGames/v1/?${params.toString()}` )
		.then( ( response ) => response.json() )
		.then( ( response ) =>
		{
			if( !response || !response.response.games || !response.response.games || response.response.games.length < 1 )
			{
				return;
			}

			optionsLink.textContent = 'Redirecting…';

			window.location = `https://steamcommunity.com/profiles/${userInfo.steamid}/stats/${response.response.games[ 0 ].appid}/achievements/`;
		} );
} );

popup.appendChild( optionsLink );
